import datetime
import os

import pytz

basedir = os.path.abspath(os.path.dirname(__file__))

SQLALCHEMY_DATABASE_URI = 'sqlite:///' + os.path.join(basedir, 'scoreboard.db')
SQLALCHEMY_MIGRATE_REPO = os.path.join(basedir, 'db_repository')
SQLALCHEMY_TRACK_MODIFICATIONS = False

SECRET_KEY = "abcdefgh"

WTF_CSRF_SECRET_KEY = "arandomstring"

# CTF specific config
DATE_FORMAT = "%a %d %H:%M %Z"
SOLVE_DATE_FORMAT = "%a %d. %X"

FILES_DIRECTORY = "/tmp/test"

CTF_NAME = "Openlab CTF"
CTF_START = datetime.datetime(2019, 8, 21, 0, 0, tzinfo=pytz.UTC)
CTF_END = datetime.datetime(2019, 8, 23, 0, 0, tzinfo=pytz.UTC)

SUBMIT_INTERVAL = 10
