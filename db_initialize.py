#!/usr/bin/env python

from scoreboard import db
from scoreboard.models import (Announcement, Solve, Task, TaskCategory,
                               File, User)


def create_user(name, password):
    db.session.add(User(name, password))
    db.session.commit()


def create_users():
    create_user("freakout", "testpw1")
    create_user("waaaaargh", "testpw2")
    create_user("tommazz", "testpw3")


def create_task_category(name):
    db.session.add(TaskCategory(name=name))
    db.session.commit()


def create_task_categories():
    create_task_category("pwning")
    create_task_category("reversing")
    create_task_category("web")


def create_task(name, description, category_name, scores, flag,
                render_files=True):
    category = TaskCategory.query.filter_by(name=category_name).first()
    task = Task(name=name, description=description, category=category,
                scores=scores, flag=flag, render_files=render_files)
    db.session.add(task)
    db.session.commit()


def create_tasks():
    create_task("pwn1", "this is pwn1", "pwning", 100, "flag_pwn1")
    create_task("rev1", "this is rev1", "reversing", 200, "flag_rev1")
    create_task("pwn3", "<p>Take this {{file|file1}}</p><pre>nc 1.2.3.4 1234</pre>",
                "pwning", 50, "flag_pwn3", render_files=False)
    create_task("pwn2", "this is pwn2", "pwning", 50, "flag_pwn2")


def create_file(taskname, filename, displayname=None):
    task = Task.query.filter_by(name=taskname).first()
    task_file = File(filename=filename, displayname=displayname)
    task.files.append(task_file)
    db.session.add(task_file)
    db.session.add(task)
    db.session.commit()


def create_task_files():
    create_file("pwn3", "file1", "vuln")
    create_file("pwn2", "file2", "libc.so")
    create_file("pwn2", "file3", "vuln")


def create_solve(username, taskname):
    user = User.query.filter_by(name=username).first()
    task = Task.query.filter_by(name=taskname).first()
    solve = Solve(solver=user, task=task)
    db.session.add(solve)
    db.session.commit()


def create_solves():
    create_solve("freakout", "pwn1")
    create_solve("freakout", "rev1")
    create_solve("waaaaargh", "rev1")
    create_solve("waaaaargh", "pwn2")


def create_announcement(title, content):
    announcement = Announcement(title=title, content=content)
    db.session.add(announcement)
    db.session.commit()


def create_announcements():
    create_announcement("announcement1", "<p>The CTF starts at  {{ctf_start}}"
                        + " and runs until {{ctf_end}}.</p>")
    create_announcement("announcement2", "<p>this is announcement2</p>")


def main():
    create_users()
    create_task_categories()
    create_tasks()
    create_task_files()
    create_solves()
    create_announcements()


if __name__ == '__main__':
    main()
