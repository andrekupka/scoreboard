# scoreboard

A scoreboard for capture-the-flag security challenges. Supports tasks in
multiple categories, user management and ranking.
