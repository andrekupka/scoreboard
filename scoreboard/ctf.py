from functools import wraps

from flask import abort, g


def ctf_state(*states):
    def _state_decorator(f):
        @wraps(f)
        def _inner(*args, **kwargs):
            if g.ctf_state not in states:
                abort(404)
            return f(*args, **kwargs)
        return _inner
    return _state_decorator


def ctf_not_finished(f):
    return ctf_state('ready', 'running')(f)


def ctf_started(f):
    return ctf_state('running', 'finished')(f)


def ctf_running(f):
    return ctf_state('running')(f)
