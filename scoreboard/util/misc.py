from functools import partial, wraps
from flask import abort, flash, g


def constant_time_compare(a, b):
    if len(a) != len(b):
        return False

    result = True
    for x, y in zip(a, b):
        if x != y:
            result = False
    return result


flash_success = partial(flash, category="success")
flash_warn = partial(flash, category="warning")
flash_info = partial(flash, category="info")
flash_error = partial(flash, category="danger")
