from flask import redirect, render_template, url_for
from flask_login import (current_user, login_required, login_user,
                         logout_user)

from . import app, db
from .ctf import ctf_not_finished, ctf_running, ctf_started
from .forms import SignInForm, SignUpForm, SubmitFlagForm
from .models import Announcement, Solve, Task, TaskCategory, User
from .util import (constant_time_compare, flash_error, flash_success,
                   flash_warn)


@app.route("/")
def index():
    return redirect(url_for("announcements"))


@app.route("/signin", methods=["GET", "POST"])
def signin():
    form = SignInForm()
    if form.validate_on_submit():
        login_user(form.get_user(), form.should_remember())
        flash_success("Successfully signed in!")
        return redirect(url_for("index"))
    return render_template("signin.html", form=form)


@app.route("/signout")
@login_required
def signout():
    logout_user()
    return redirect(url_for("index"))


@app.route("/signup", methods=["GET", "POST"])
@ctf_not_finished
def signup():
    form = SignUpForm()
    if form.validate_on_submit():
        new_user = User(form.username.data, form.password.data)
        db.session.add(new_user)
        db.session.commit()
        login_user(new_user)
        flash_success("Successfully signed up!")
        return redirect(url_for("index"))
    return render_template("signup.html", form=form)


@app.route("/announcements")
def announcements():
    announcements = Announcement.get_chronological_announcements()
    return render_template("announcements.html", announcements=announcements)


@app.route("/announcement/<int:announcement_id>")
def announcement(announcement_id):
    announcement = Announcement.query.get_or_404(announcement_id)
    return render_template("announcement.html", announcement=announcement)


@app.route("/scoreboard")
@ctf_started
def scoreboard():
    tasks = Task.scoreboard_tasks()
    ranked_users = User.get_ranked_users()
    return render_template("scoreboard.html", tasks=tasks,
                           ranked_users=ranked_users)


@app.route("/user/<int:user_id>")
def user(user_id):
    return render_template("user.html", user=User.query.get_or_404(user_id))


@app.route("/tasks")
@ctf_started
def tasks():
    categories = TaskCategory.with_tasks_and_solve_count()
    return render_template("tasks.html", categories=categories,
                           form=SubmitFlagForm())


@app.route("/tasks/submit/<int:task_id>", methods=["POST"])
@ctf_running
@login_required
def submit_flag(task_id):
    form = SubmitFlagForm()
    if not current_user.try_and_update_submit():
        flash_warn("You can only submit a new flag every %d seconds!" %
                   app.config['SUBMIT_INTERVAL'])
        return redirect(url_for("tasks"))
    if not form.validate_on_submit():
        flash_warn("An empty flag cannot be submitted!")
        return redirect(url_for("tasks"))

    task = Task.query.get_or_404(task_id)
    if current_user.has_task_solved(task):
        flash_success("You have already solved this task!")
    elif constant_time_compare(task.flag, form.flag.data):
        db.session.add(Solve(solver=current_user, task=task))
        db.session.commit()
        flash_success("The flag was correct!")
    else:
        flash_error("The flag was incorrect!")
    return redirect(url_for("tasks"))
