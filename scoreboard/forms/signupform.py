from flask_wtf import Form
from wtforms import PasswordField, StringField
from wtforms.validators import DataRequired

from ..models import User
from ..util import flash_error


class SignUpForm(Form):
    username = StringField("username", validators=[DataRequired()])
    password = PasswordField("password", validators=[DataRequired()])
    confirm_password = PasswordField("confirm_password",
                                     validators=[DataRequired()])

    def __init__(self, *args, **kwargs):
        super(SignUpForm, self).__init__(*args, **kwargs)

    def validate(self):
        if not Form.validate(self):
            return False

        if self._user_exists():
            flash_error("User '%s' already exists!" % self.username.data)
            return False
        if self.password.data != self.confirm_password.data:
            flash_error('Passwords do not match!')
            return False
        return True

    def _user_exists(self):
        user = User.query.filter_by(name=self.username.data).first()
        return user is not None
