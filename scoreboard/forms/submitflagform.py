from flask_wtf import Form
from wtforms import StringField
from wtforms.validators import DataRequired


class SubmitFlagForm(Form):
    flag = StringField("flag", validators=[DataRequired()])

    def __init__(self, *args, **kwargs):
        super(SubmitFlagForm, self).__init__(*args, **kwargs)
