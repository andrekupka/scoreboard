from flask_wtf import Form
from wtforms import BooleanField, PasswordField, StringField
from wtforms.validators import DataRequired

from ..models import User
from ..util import flash_error


class SignInForm(Form):
    username = StringField("username", validators=[DataRequired()])
    password = PasswordField("password", validators=[DataRequired()])
    remember_me = BooleanField("remember_me", default=False)

    def __init__(self, *args, **kwargs):
        super(SignInForm, self).__init__(*args, **kwargs)

    def validate(self):
        if not Form.validate(self):
            return False

        user = self.get_user()
        if user is None or not user.is_password_valid(self.password.data):
            flash_error("Invalid username or password!")
            return False
        return True

    def get_user(self):
        return User.query.filter_by(name=self.username.data).first()

    def should_remember(self):
        return self.remember_me.data
