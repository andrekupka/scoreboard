import datetime
import hashlib
import os

import pytz
from sqlalchemy.sql import func

from .solve import Solve
from .task import Task, TaskCategory
from .. import app, db, lm
from ..util import constant_time_compare


class RankedUser:
    def __init__(self, user, solves, score):
        self.user = user
        self.name = user.name
        self.solves = solves
        self.score = score


class User(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(64), index=True, unique=True)
    password_hash = db.Column(db.String(64))
    salt = db.Column(db.String(128))
    last_submit_time = db.Column(db.DateTime(timezone=True),
                                 default=datetime.datetime.utcfromtimestamp(0))
    solves = db.relationship('Solve', backref='solver', lazy='dynamic')

    def __init__(self, name, password):
        self.name = name
        self.salt = os.urandom(64).hex()
        self.password_hash = self._calculate_hash(password)

    def _calculate_hash(self, password):
        h = hashlib.sha256()
        h.update(self.salt.encode() + password.encode())
        return h.hexdigest()

    def is_password_valid(self, password):
        other_hash = self._calculate_hash(password)
        return constant_time_compare(other_hash, self.password_hash)

    @property
    def is_authenticated(self):
        return True

    @property
    def is_active(self):
        return True

    @property
    def is_anonymous(self):
        return False

    def get_id(self):
        return str(self.id)

    def __repr__(self):
        return "User<name=%s>" % self.name

    def get_rank(self):
        for index, ranked_user in enumerate(User.get_ranked_users()):
            if self.id == ranked_user.user.id:
                return index + 1
        raise Exception("User has no rank")

    def get_chronological_solves(self):
        return self.solves.join(Task, TaskCategory) \
            .order_by(Solve.timestamp.desc(), TaskCategory.name, Task.scores,
                      Task.name)

    def has_task_solved(self, task):
        return self.solves.filter_by(task=task).count() > 0

    def try_and_update_submit(self):
        now = datetime.datetime.utcnow()
        delta = now - self.last_submit_time
        if delta.total_seconds() < app.config["SUBMIT_INTERVAL"]:
            return False
        self.last_submit_time = now
        db.session.add(self)
        db.session.commit()
        return True

    @classmethod
    def get_ranked_users(cls):
        tasks = Task.scoreboard_tasks()
        users = cls.query.outerjoin(Solve, Task).add_column(func.sum(Task.scores)) \
            .group_by(User)

        ranked_users = list()
        for user, score in users:
            ranked_users.append(RankedUser(user,
                                           User.get_task_solves(user, tasks),
                                           score or 0))
        return sorted(ranked_users, key=cls._compute_ranking_value,
                      reverse=True)

    @staticmethod
    def get_task_solves(user, tasks):
        # TODO this implementation is really ugly, try to improve this
        task_solves = list()
        sorted_solves = user.solves.join(Task, TaskCategory) \
            .order_by(Task.category, Task.scores, Task.name).all()
        solve_index = 0
        for task in tasks:
            solved_task = None
            if solve_index < len(sorted_solves):
                solved_task = sorted_solves[solve_index].task
            if solved_task == task:
                task_solves.append(True)
                solve_index += 1
            else:
                task_solves.append(False)
        return task_solves

    @classmethod
    def _compute_ranking_value(cls, ranked_user):
        time = 0
        for solve in ranked_user.user.solves:
            delta = (solve.timestamp.replace(tzinfo=pytz.UTC) -
                     app.config["CTF_START"])
            time += int(delta.total_seconds() * 1000)
        return (ranked_user.score or 0, -time)


@lm.user_loader
def load_user(id):
    return User.query.get(int(id))
