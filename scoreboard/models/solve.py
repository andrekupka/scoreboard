from sqlalchemy.sql import func
from .. import db


class Solve(db.Model):
    solver_id = db.Column(db.Integer, db.ForeignKey('user.id'),
                          primary_key=True)
    task_id = db.Column(db.Integer, db.ForeignKey('task.id'), primary_key=True)
    timestamp = db.Column(db.DateTime(timezone=True), default=func.now())

    def __repr__(self):
        return ("Solve<solver=%r,task=%r,timestamp=%s>" %
                (self.solver, self.task, self.timestamp))
