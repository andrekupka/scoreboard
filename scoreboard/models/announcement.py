from sqlalchemy.sql import func

from .. import db


class Announcement(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    title = db.Column(db.String(64))
    content = db.Column(db.Text)
    creation_time = db.Column(db.DateTime, default=func.now())

    @classmethod
    def get_chronological_announcements(cls):
        return cls.query.order_by(cls.creation_time.desc())

    def __repr__(self):
        return ("Announcement<title=%s,creation_time=%r>" %
                (self.title, self.creation_time))
