from .. import db
from .file import File
from .solve import Solve


task_files = db.Table('task_files', db.metadata,
                      db.Column('task_id', db.Integer, db.ForeignKey('task.id')),
                      db.Column('file_id', db.Integer, db.ForeignKey('file.id')))


class TaskCategory(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(64), unique=True)
    tasks = db.relationship("Task", backref="category", lazy="dynamic")

    def __repr__(self):
        return "TaskCategory<name=%s>" % self.name

    def get_ordered_tasks(self):
        return self.tasks.order_by(Task.scores, Task.name)

    @classmethod
    def with_tasks_and_solve_count(cls):
        return cls.query.join(cls.tasks).order_by(cls.name)


class Task(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(64), unique=True)
    description = db.Column(db.Text)
    scores = db.Column(db.Integer)
    flag = db.Column(db.String(64), index=True, unique=True)
    category_id = db.Column(db.ForeignKey('task_category.id'))
    render_files = db.Column(db.Boolean, default=True)
    solves = db.relationship("Solve", backref="task", lazy="dynamic")
    files = db.relationship("File", secondary=task_files, backref="tasks",
                            lazy="dynamic")

    def __repr__(self):
        return ("Task<name=%s,scores=%d,flag=%s,category=%r>" %
                (self.name, self.scores, self.flag, self.category))

    def get_solve_count(self):
        return self.solves.count()

    def get_ordered_files(self):
        # TODO nulls last
        return self.files.order_by(File.displayname)

    def has_files(self):
        return self.files.count() > 0

    @classmethod
    def scoreboard_tasks(cls):
       return cls.query.join(cls.category).order_by(TaskCategory.name,
                                                    cls.scores,
                                                    cls.name)
