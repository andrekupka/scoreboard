from .. import db


class File(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    filename = db.Column(db.String(128))
    displayname = db.Column(db.String(64))
