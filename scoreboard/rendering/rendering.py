from .. import app


@app.template_filter('render_datetime')
def render_datetime(datetime, solve=False):
    if solve:
        return datetime.strftime(app.config["SOLVE_DATE_FORMAT"])
    return datetime.strftime(app.config["DATE_FORMAT"])
