import re
from functools import partial

from flask import url_for

from .rendering import render_datetime
from .. import app
from ..models import File


COMMAND_REGEX = re.compile("\\{\\{(\\w+)(\\|(\\w+))?\\}\\}")


@app.template_filter('render_content')
def render_content(content):
    length_decrease = 0
    for match in COMMAND_REGEX.finditer(content):
        rendered = process_command(match.group(1), match.group(3)) or ""
        content = (content[:match.start() - length_decrease] + rendered +
                   content[match.end() - length_decrease:])
        length_decrease += (match.end() - match.start()) - len(rendered)
    return content


def process_command(command, param):
    command_object = COMMANDS.get(command)
    if command_object:
        return command_object(command, param)
    return None


LINK_TEMPLATE = '<a href="%s">%s</a>'


def render_file(command, filename, use_url):
    target_file = File.query.filter_by(filename=filename).first()

    if target_file is None:
        return None

    url = url_for("files.static", filename=target_file.filename)
    filename = target_file.displayname
    if use_url or filename is None:
        filename = url
    return LINK_TEMPLATE % (url, filename)


COMMANDS = {
    'file': partial(render_file, use_url=False),
    'fileurl': partial(render_file, use_url=True),
    'ctf_start': lambda c, p: render_datetime(datetime=app.config['CTF_START'],
                                              solve=p),
    'ctf_end': lambda c, p: render_datetime(datetime=app.config['CTF_END'],
                                            solve=p)
}


