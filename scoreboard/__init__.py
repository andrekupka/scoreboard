import datetime
from sqlite3 import Connection as SQLite3Connection

import pytz
from flask import g, Blueprint, Flask
from flask_login import LoginManager
from flask_sqlalchemy import SQLAlchemy
from flask_wtf.csrf import CSRFProtect
from sqlalchemy import event
from sqlalchemy.engine import Engine

app = Flask("scoreboard")
app.config.from_object('config')
csrf = CSRFProtect(app)
db = SQLAlchemy(app)

static_files = Blueprint('files', __name__, static_url_path="/files",
                         static_folder=app.config["FILES_DIRECTORY"])
app.register_blueprint(static_files)

lm = LoginManager()
lm.init_app(app)
lm.login_view = "signin"


@app.before_request
def set_ctf_state():
    # TODO UTC
    current_time = datetime.datetime.now(pytz.UTC)
    ctf_start = app.config['CTF_START']
    ctf_end = app.config['CTF_END']
    if current_time < ctf_start:
        g.ctf_state = 'ready'
    elif current_time >= ctf_start and current_time < ctf_end:
        g.ctf_state = 'running'
    else:
        g.ctf_state = 'finished'


@event.listens_for(Engine, "connect")
def _set_sqlite_pragma(dbapi_connection, connection_record):
    if isinstance(dbapi_connection, SQLite3Connection):
        cursor = dbapi_connection.cursor()
        cursor.execute("PRAGMA foreign_keys=ON;")
        cursor.close()


from .rendering import *
from .models import *
from .views import *
